# AI_sf2hf

### Overview:

AI_sf2hf is a plugin for the MAME emulator that uses a neural network that learns how to play the popular arcade game Street Fighter 2.  AI_sf2hf is written in Lua.


### Instructions:

Installation and setup of MAME is beyond the scope of this document.  You will require the "Street Fighter 2: Hyper Fighting (World 921209)"/"sf2hf" ROM file.  No other game version will work as memory addresses differ between versions!

Copy the "AI_sf2hf" folder into the MAME plugins folder.

To launch MAME with this plugin, use the following command line:
> mame64 sf2hf -w -skip_gameinfo -autoboot_script plugins\AI_sf2hf\AI_sf2hf.lua

Use the "-speed N" command line option to run the emulator faster than default.  For example "-speed 6.0" will run at 6x the speed.  Alternatively use "-nothrottle" to run at the maximum possible speed.  Also useful are the "-sound none" and "-video none" command line options which will run the emulator without a display.


### Neural Network/Genetic Algorithm Overview:

This AI is controlled by a neural network that accepts a variety of game data as inputs and outputs joystick movement and button presses.  The inputs to the network include the health of each character, distance between characters, projectiles, and more.  Based on this input, the network will determine whether to activate the joystick or any of the six attack buttons.  

At the start, a number of networks (genomes) are created and populated at random.  In order to "learn", a genetic algorithm is used to judge the "fitness" of each genome in the current generation.  Then a number of the most "fit" genomes are selected to act as "parents" to create a new generation of networks.  This process continues to ultimately create networks that can most suit the task - that is to be able to defeat the game's built-in computer opponent.

For example, a completely random network in the first generation may not be effective at all...

<img src="https://gitlab.com/dannmoore/ai_sf2hf/-/raw/master/media/animated1.gif" alt="animated" width="640"/>

After a while of learning the network can put up a better fight...

<img src="https://gitlab.com/dannmoore/ai_sf2hf/-/raw/master/media/animated2.gif" alt="animated" width="640"/>

Much later we can see the network holding the opponent at bay with fireballs, and countering aerial attacks...

<img src="https://gitlab.com/dannmoore/ai_sf2hf/-/raw/master/media/animated3.gif" alt="animated" width="640"/>


### Notes:

- The network currently only supports playing as Ryu
- It is well known that the computer player in Street Fighter 2 actively cheated in several ways, and this may affect the behavior of the network
- Due to the nature of the arcade game, the player faces a new opponent after winning two rounds.  The network will then need to learn how to play against that opponent as well, so this will require multiple playthroughs to learn how to fight against each opponent.
- The current generation is automatically saved when created, and also upon exit.  Depending on the size of the neural network it may take hours or even days of gameplay to learn how to be effective.  The genomes will be loaded at next launch so multiple sessions can be used without losing progress.  


### Legal:
Copyright (C) 2024 Dann Moore.  All rights reserved.





