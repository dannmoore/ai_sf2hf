--[[
  File:    AI_sf2hf.lua
  Description: A plugin for MAME that implements an artificial intelligence to play Street Fighter 2
  Notes:

  Copyright (C) 2020 Dann Moore.  All rights reserved.
--]]





--
-- Debug
--
-- Print main screen width/height
--print("Screen Width: " .. manager:machine().screens[":screen"]:width())
--print("Screen Height: " .. manager:machine().screens[":screen"]:height())





--
-- Global Data
--

-- Gather all buttons available
local buttons0 = {}
local buttons1 = {}
local buttons2 = {}
local switchesB = {}
local ports = manager:machine():ioport().ports
--for k,v in pairs(ports) do print(k) end

local IN0 = ports[":IN0"]
--print("Port 0 Button Names:")
for field_name, field in pairs(IN0.fields) do
    buttons0[field_name] = field
    --print(buttons0[field_name].name)
end
local IN1 = ports[":IN1"]
--print("Port 1 Button Names:")
for field_name, field in pairs(IN1.fields) do
    buttons1[field_name] = field
    --print(buttons1[field_name].name)
end
local IN2 = ports[":IN2"]
--print("Port 2 Button Names:")
for field_name, field in pairs(IN2.fields) do
    buttons2[field_name] = field
    --print(buttons2[field_name].name)
end

local DSWB = ports[":DSWB"]
--print("Dip Switches B Names:")
for field_name, field in pairs(DSWB.fields) do
    switchesB[field_name] = field
    --print(switchesB[field_name].name)
end



math.randomseed(os.time()) -- Seed RNG with OS time


-- A random amount to vary the game start.  This will vary any timer-based RNG in the emulated machine
local frame_randomstart = math.random(0,30) 
local frame_num = 0
local frame_throttle = 4  -- Throttle AI response to every N frames (reduces reaction time and attack spamming)
local game_started = false
local continue_game = false
local frame_continue = 0
local endround_processed = false	-- Has the end of the round been processed yet?

local P1PreviousHealth = 0	-- Health of Player 1 in the previous frame
local P2PreviousHealth = 0	-- Health of Player 2 in the previous frame

local P1Wins = 0	-- Number of Player 1 wins in round
local P2Wins = 0	-- Number of Player 2 wins in round





--
-- Game-related memory functions
--

-- Note that these memory addresses may change between versions of this game!
--
-- Game data begins @ 0xFF844E
-- Continue timer @ 0xFF8645, 1 byte, 0-9

function P1_Enabled()
	if manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF86EE) == 1 then
		return true
	end

	return false
end


function P2_Enabled()
	if manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF89EE) == 1 then
		return true
	end

	return false
end


function P1_GetHealth()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF83E8)
end

function P2_GetHealth()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF86E8)
end


function P1_GetPositionX()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF858A)
end

function P1_GetPositionY()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF858E)
end


function P2_GetPositionX()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF888A)
end

function P2_GetPositionY()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF888E)
end


--[[ Character Table:
	Ryu = 0x00
	E. Honda = 0x01
	Blanka = 0x02
	Guile = 0x03
	Ken = 0x04
	Chun Li = 0x05
	Zangief = 0x06
	Dhalsim = 0x07
	M. Bison = 0x08
	Sagat = 0x09
	Balrog = 0x0A
	Vega = 0x0B
--]]
function P1_GetCharacter()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF864F)
end

function P2_GetCharacter()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF894F)
end


--[[
	Character state table:
	0x00 = Standing
	0x02 = Crouching 
	0x04 = Jumping
	0x06 = Jump-crouch
	0x08 = Blocking
	0x0A = Attacking
	0x0E = Stunned
	0x14 = On Ground
--]]
function P1_GetState()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF83C1)
end

function P2_GetState()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF86C1)
end


-- There can be up to 8 projectiles at a time (slots 0-7)
-- The game uses slot 7 first, then slot 6, and soforth
function ProjectileActive(slotnum)
	if manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF9387 + (0xC0 * slotnum)) ~= 0 then
		return true
	else
		return false
	end
end

function ProjectilePositionX(slotnum)
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF937C + (0xC0 * slotnum))
end

function ProjectilePositionY(slotnum)
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF9380 + (0xC0 * slotnum))
end



function GetTimer()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF8ABE)
end



function GetRoundDone()
	if manager:machine().devices[":maincpu"].spaces["program"]:read_u16(0xFF8AC0) == 1 then
		return true
	end

	return false
end

-- 0 = no winner, 1 = Player 1, 2 = Player 2, 255 = Draw
function GetRoundWinner()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF8AC2)	
end


function GetContinueTime()
	return manager:machine().devices[":maincpu"].spaces["program"]:read_u8(0xFF8645)	
end








--
-- Neural network
--
local AI_NN = require("AI_sf2hf/AI_NN")
local NN = {}
local sensitivity = 0.75 -- How much of a signal on the output nodes do we need to activate a given button



-- To assist the AI keep track of the current step in the sequence
local combo_move_step = {}
-- Use the index that matches the neural network output layer for each special combo
-- Initialize to -1 to indicate the combo move is not currently being performed
combo_move_step[11] = -1 -- Down, down-right, right, jab punch (fireball motion)
combo_move_step[12] = -1 -- Right, down, down-right, jab punch (dragon punch motion)	
combo_move_step[13] = -1 -- Down, down-left, left, short kick (hurricane kick motion)

combo_move_step[14] = -1 -- Down, down-left, left, jab punch (fireball motion)
combo_move_step[15] = -1 -- left, down, down-left, jab punch (dragon punch motion)	
combo_move_step[16] = -1 -- Down, down-right, right, short kick (hurricane kick motion)

combo_move_step[17] = -1 -- Down, down-right, right, strong punch (fireball motion)
combo_move_step[18] = -1 -- Right, down, down-right, strong punch (dragon punch motion)	
combo_move_step[19] = -1 -- Down, down-left, left, forward kick (hurricane kick motion)

combo_move_step[20] = -1 -- Down, down-left, left, strong punch (fireball motion)
combo_move_step[21] = -1 -- left, down, down-left, strong punch (dragon punch motion)	
combo_move_step[22] = -1 -- Down, down-right, right, forward kick (hurricane kick motion)

combo_move_step[23] = -1 -- Down, down-right, right, fierce punch (fireball motion)
combo_move_step[24] = -1 -- Right, down, down-right, fierce punch (dragon punch motion)	
combo_move_step[25] = -1 -- Down, down-left, left, roundhouse kick (hurricane kick motion)

combo_move_step[26] = -1 -- Down, down-left, left, fierce punch (fireball motion)
combo_move_step[27] = -1 -- left, down, down-left, fierce punch (dragon punch motion)	
combo_move_step[28] = -1 -- Down, down-right, right, roundhouse kick (hurricane kick motion)




--
-- Genetic Algorithm
--
local AI_GA = require("AI_sf2hf/AI_GA")
local GA = AI_GA:new()

local fitness = {}	-- Fitness of the each genome
local genomes = {}	-- Genomes in the current generation

local savefile = "plugins/AI_sf2hf/genome.txt"  -- Output file path/name to save and load 

local generation_wins = 0		-- Track wins/losses/draws per generation
local generation_losses = 0
local generation_draws = 0

local current_generation = 1	-- Which generation of neural network are we currently processing
local current_genome = 1 		-- Which genome are we currently processing
local total_genomes = 50		-- Total number of genomes per generation
local num_fitparents = 6		-- The fittest N genomes will be chosen to breed the next generation
local num_randomparents = 1		-- N of the remaining genomes will be chosen at random to also breed the next generation
local mutationrate = 150		-- % of the genome that will be mutated (on average).  Note that this is in 100ths of a percent, so
								--  a mutationrate of 1 equals a .01% chance of mutation _per gene_.




-- Create a neural network 
NN = AI_NN:new()
-- Multiple hidden layers can be used...
NN:AddLayer(57)	-- Input layer
NN:AddLayer(35)	-- Hidden layer
NN:AddLayer(14)	-- Hidden layer
NN:AddLayer(28)	-- Output layer

--[[ Input Layer Chart:
 		Node		Description
		----		-----------
		1			Player 1 Health 
		2			Player 2 Health
		3			Player 1 Facing
		4-15		Player 2 Character (each node is "on" or "off" to indicate which character)
		16-23		Player 1 State (standing, jumping, etc) (each node is "on" or "off" to indicate which state)
		24-31		Player 2 State (standing, jumping, etc) (each node is "on" or "off" to indicate which state)
		32			X Distance Between Characters
		33			Y Distance Between Characters			
		34-57		Projectile Slots 0-7, Active, X Distance to Character, Y Distance to Character
--]]
		
--[[ Output Layer Chart:
 		Node		Description
		----		-----------
		1			Joystick Up
		2			Joystick Down
		3			Joystick Left
		4			Joystick Right
		5			Jab Punch
		6			Strong Punch
		7			Fierce Punch
		8			Short Kick
		9			Forward Kick
		10			Roundhouse Kick
		11-28		Special moves that require a combination of button presses
--]]





-- Setup each genome by generating random data in a neural network, then storing it in the genomes table
for i = 1, total_genomes do
	NN:Prepare()		
	genomes[i] = NN:Serialize()
end



--
-- Saving/Loading 
--

--
-- SaveGenomes() - Saves genomes to the specified file
--
function SaveGenomes(filename)
	file = io.open(filename, "w")
	io.output(file)

	-- Write generation info
	str = current_generation
	io.write(str .. "\n")

	-- Loop through each genome
	for i = 1, total_genomes do
		-- Write each genome to file, one value per line
		for j = 1, #genomes[i] do
			str = genomes[i][j]
			io.write(str .. "\n")
		end
	end

	io.close(file)
end


--
-- LoadGenomes() - Loads genomes from the specified file. Note that if the structure of the neural
--  network or number of genomes has changed, you may encounter errors from an out-of-date
--  save.  Be sure to delete the old saved data before running again.
--
function LoadGenomes(filename)
	file = io.open(filename, "r")
	if file ~= nil then -- Only load if file exists
		io.input(file)

		-- Read generation info
		str = io.read()
		current_generation = tonumber(str)

		-- Loop through each genome
		for i = 1, total_genomes do
			-- Read each value from file
			for j = 1, #genomes[i] do
				str = io.read()
				genomes[i][j] = tonumber(str)
			end
		end

		io.close(file)
	end
end


--
-- FileExists() - Checks if the given file exists, returns true if the file exists
--
function FileExists(filename)
	file = io.open(filename, "r")
	if file ~= nil then 
		return true
	else
		return false
	end
end











--
-- AI
--


-- Keep track of the input buttons that the AI will use and their on/off state
local AIButtons = {}
AIButtons["P1 Up"] = { button = buttons1["P1 Up"], pressed = false }
AIButtons["P1 Down"] = { button = buttons1["P1 Down"], pressed = false }
AIButtons["P1 Left"] = { button = buttons1["P1 Left"], pressed = false }
AIButtons["P1 Right"] = { button = buttons1["P1 Right"], pressed = false }
AIButtons["P1 Jab Punch"] = { button = buttons1["P1 Jab Punch"], pressed = false }
AIButtons["P1 Strong Punch"] = { button = buttons1["P1 Strong Punch"], pressed = false }
AIButtons["P1 Fierce Punch"] = { button = buttons1["P1 Fierce Punch"], pressed = false }
AIButtons["P1 Short Kick"] = { button = buttons2["P1 Short Kick"], pressed = false }
AIButtons["P1 Forward Kick"] = { button = buttons2["P1 Forward Kick"], pressed = false }
AIButtons["P1 Roundhouse Kick"] = { button = buttons2["P1 Roundhouse Kick"], pressed = false }



--
-- CheckComboMoves() - Checks to see if any combo moves are in Progress
--  Returns -1 if no combo moves are in progress, otherwise returns the combo_move_step index of the active move
--
function CheckComboMoves()
	retval = -1

	for i = 11, 28 do
		if combo_move_step[i] > 0 then
			retval = i
			break;
		end
	end

	return retval
end


--
-- ProcessComboMoves() - Processes the combo move in progress, injecting the appropriate actions
--
function ProcessComboMoves(index)
	alldone = false

	if index == 11 then -- fireball right
		if combo_move_step[11] == 1 then -- Starting a new combo move
			combo_move_step[11] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[11] == 2 then -- Continuing this combo move
			combo_move_step[11] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[11] == 3 then -- Continuing this combo move
			combo_move_step[11] = 4
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[11] == 4 then -- Continuing this combo move
			combo_move_step[11] = 5
			AIButtons["P1 Jab Punch"].pressed = true
		elseif combo_move_step[11] == 5 then -- Cooldown frame
			alldone = true
		end

	elseif index == 12 then -- dragon punch right
		if combo_move_step[12] == 1 then -- Starting a new combo move
			combo_move_step[12] = 2
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[12] == 2 then -- Continuing this combo move
			combo_move_step[12] = 3
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[12] == 3 then -- Continuing this combo move
			combo_move_step[12] = 4
			AIButtons["P1 Down"].pressed = true				
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[12] == 4 then -- Continuing this combo move
			combo_move_step[12] = 5
			AIButtons["P1 Jab Punch"].pressed = true
		elseif combo_move_step[12] == 5 then -- Cooldown frame	
			alldone = true
		end

	elseif index == 13 then -- hurricane kick right
		if combo_move_step[13] == 1 then -- Starting a new combo move
			combo_move_step[13] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[13] == 2 then -- Continuing this combo move
			combo_move_step[13] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Left"].pressed = true				
		elseif combo_move_step[13] == 3 then -- Continuing this combo move
			combo_move_step[13] = 4
			AIButtons["P1 Left"].pressed = true				
		elseif combo_move_step[13] == 4 then -- Continuing this combo move
			combo_move_step[13] = 5
			AIButtons["P1 Short Kick"].pressed = true
		elseif combo_move_step[13] == 5 then -- Cooldown frame		
			alldone = true		
		end

	elseif index == 14 then -- fireball left
		if combo_move_step[14] == 1 then -- Starting a new combo move
			combo_move_step[14] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[14] == 2 then -- Continuing this combo move
			combo_move_step[14] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[14] == 3 then -- Continuing this combo move
			combo_move_step[14] = 4
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[14] == 4 then -- Continuing this combo move
			combo_move_step[14] = 5
			AIButtons["P1 Jab Punch"].pressed = true
		elseif combo_move_step[14] == 5 then -- Cooldown frame
			alldone = true
		end

	elseif index == 15 then -- dragon punch left
		if combo_move_step[15] == 1 then -- Starting a new combo move
			combo_move_step[15] = 2
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[15] == 2 then -- Continuing this combo move
			combo_move_step[15] = 3
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[15] == 3 then -- Continuing this combo move
			combo_move_step[15] = 4
			AIButtons["P1 Down"].pressed = true				
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[15] == 4 then -- Continuing this combo move
			combo_move_step[15] = 5
			AIButtons["P1 Jab Punch"].pressed = true
		elseif combo_move_step[15] == 5 then -- Cooldown frame	
			alldone = true
		end

	elseif index == 16 then -- hurricane kick left
		if combo_move_step[16] == 1 then -- Starting a new combo move
			combo_move_step[16] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[16] == 2 then -- Continuing this combo move
			combo_move_step[16] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Right"].pressed = true				
		elseif combo_move_step[16] == 3 then -- Continuing this combo move
			combo_move_step[16] = 4
			AIButtons["P1 Right"].pressed = true				
		elseif combo_move_step[16] == 4 then -- Continuing this combo move
			combo_move_step[16] = 5
			AIButtons["P1 Short Kick"].pressed = true
		elseif combo_move_step[16] == 5 then -- Cooldown frame		
			alldone = true		
		end

	elseif index == 17 then -- fireball right
		if combo_move_step[17] == 1 then -- Starting a new combo move
			combo_move_step[17] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[17] == 2 then -- Continuing this combo move
			combo_move_step[17] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[17] == 3 then -- Continuing this combo move
			combo_move_step[17] = 4
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[17] == 4 then -- Continuing this combo move
			combo_move_step[17] = 5
			AIButtons["P1 Strong Punch"].pressed = true
		elseif combo_move_step[17] == 5 then -- Cooldown frame
			alldone = true
		end

	elseif index == 18 then -- dragon punch right
		if combo_move_step[18] == 1 then -- Starting a new combo move
			combo_move_step[18] = 2
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[18] == 2 then -- Continuing this combo move
			combo_move_step[18] = 3
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[18] == 3 then -- Continuing this combo move
			combo_move_step[18] = 4
			AIButtons["P1 Down"].pressed = true				
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[18] == 4 then -- Continuing this combo move
			combo_move_step[18] = 5
			AIButtons["P1 Strong Punch"].pressed = true
		elseif combo_move_step[18] == 5 then -- Cooldown frame	
			alldone = true
		end

	elseif index == 19 then -- hurricane kick right
		if combo_move_step[19] == 1 then -- Starting a new combo move
			combo_move_step[19] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[19] == 2 then -- Continuing this combo move
			combo_move_step[19] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Left"].pressed = true				
		elseif combo_move_step[19] == 3 then -- Continuing this combo move
			combo_move_step[19] = 4
			AIButtons["P1 Left"].pressed = true				
		elseif combo_move_step[19] == 4 then -- Continuing this combo move
			combo_move_step[19] = 5
			AIButtons["P1 Forward Kick"].pressed = true
		elseif combo_move_step[19] == 5 then -- Cooldown frame		
			alldone = true		
		end

	elseif index == 20 then -- fireball left
		if combo_move_step[20] == 1 then -- Starting a new combo move
			combo_move_step[20] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[20] == 2 then -- Continuing this combo move
			combo_move_step[20] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[20] == 3 then -- Continuing this combo move
			combo_move_step[20] = 4
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[20] == 4 then -- Continuing this combo move
			combo_move_step[20] = 5
			AIButtons["P1 Strong Punch"].pressed = true
		elseif combo_move_step[20] == 5 then -- Cooldown frame
			alldone = true
		end

	elseif index == 21 then -- dragon punch left
		if combo_move_step[21] == 1 then -- Starting a new combo move
			combo_move_step[21] = 2
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[21] == 2 then -- Continuing this combo move
			combo_move_step[21] = 3
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[21] == 3 then -- Continuing this combo move
			combo_move_step[21] = 4
			AIButtons["P1 Down"].pressed = true				
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[21] == 4 then -- Continuing this combo move
			combo_move_step[21] = 5
			AIButtons["P1 Strong Punch"].pressed = true
		elseif combo_move_step[21] == 5 then -- Cooldown frame	
			alldone = true
		end

	elseif index == 22 then -- hurricane kick left
		if combo_move_step[22] == 1 then -- Starting a new combo move
			combo_move_step[22] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[22] == 2 then -- Continuing this combo move
			combo_move_step[22] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Right"].pressed = true				
		elseif combo_move_step[22] == 3 then -- Continuing this combo move
			combo_move_step[22] = 4
			AIButtons["P1 Right"].pressed = true				
		elseif combo_move_step[22] == 4 then -- Continuing this combo move
			combo_move_step[22] = 5
			AIButtons["P1 Forward Kick"].pressed = true
		elseif combo_move_step[22] == 5 then -- Cooldown frame		
			alldone = true		
		end

	elseif index == 23 then -- fireball right
		if combo_move_step[23] == 1 then -- Starting a new combo move
			combo_move_step[23] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[23] == 2 then -- Continuing this combo move
			combo_move_step[23] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[23] == 3 then -- Continuing this combo move
			combo_move_step[23] = 4
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[23] == 4 then -- Continuing this combo move
			combo_move_step[23] = 5
			AIButtons["P1 Fierce Punch"].pressed = true
		elseif combo_move_step[23] == 5 then -- Cooldown frame
			alldone = true
		end

	elseif index == 24 then -- dragon punch right
		if combo_move_step[24] == 1 then -- Starting a new combo move
			combo_move_step[24] = 2
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[24] == 2 then -- Continuing this combo move
			combo_move_step[24] = 3
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[24] == 3 then -- Continuing this combo move
			combo_move_step[24] = 4
			AIButtons["P1 Down"].pressed = true				
			AIButtons["P1 Right"].pressed = true
		elseif combo_move_step[24] == 4 then -- Continuing this combo move
			combo_move_step[24] = 5
			AIButtons["P1 Fierce Punch"].pressed = true
		elseif combo_move_step[24] == 5 then -- Cooldown frame	
			alldone = true
		end

	elseif index == 25 then -- hurricane kick right
		if combo_move_step[25] == 1 then -- Starting a new combo move
			combo_move_step[25] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[25] == 2 then -- Continuing this combo move
			combo_move_step[25] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Left"].pressed = true				
		elseif combo_move_step[25] == 3 then -- Continuing this combo move
			combo_move_step[25] = 4
			AIButtons["P1 Left"].pressed = true				
		elseif combo_move_step[25] == 4 then -- Continuing this combo move
			combo_move_step[25] = 5
			AIButtons["P1 Roundhouse Kick"].pressed = true
		elseif combo_move_step[25] == 5 then -- Cooldown frame		
			alldone = true		
		end

	elseif index == 26 then -- fireball left
		if combo_move_step[26] == 1 then -- Starting a new combo move
			combo_move_step[26] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[26] == 2 then -- Continuing this combo move
			combo_move_step[26] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[26] == 3 then -- Continuing this combo move
			combo_move_step[26] = 4
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[26] == 4 then -- Continuing this combo move
			combo_move_step[26] = 5
			AIButtons["P1 Fierce Punch"].pressed = true
		elseif combo_move_step[26] == 5 then -- Cooldown frame
			alldone = true
		end

	elseif index == 27 then -- dragon punch left
		if combo_move_step[27] == 1 then -- Starting a new combo move
			combo_move_step[27] = 2
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[27] == 2 then -- Continuing this combo move
			combo_move_step[27] = 3
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[27] == 3 then -- Continuing this combo move
			combo_move_step[27] = 4
			AIButtons["P1 Down"].pressed = true				
			AIButtons["P1 Left"].pressed = true
		elseif combo_move_step[27] == 4 then -- Continuing this combo move
			combo_move_step[27] = 5
			AIButtons["P1 Fierce Punch"].pressed = true
		elseif combo_move_step[27] == 5 then -- Cooldown frame	
			alldone = true
		end

	elseif index == 28 then -- hurricane kick left
		if combo_move_step[28] == 1 then -- Starting a new combo move
			combo_move_step[28] = 2
			AIButtons["P1 Down"].pressed = true
		elseif combo_move_step[28] == 2 then -- Continuing this combo move
			combo_move_step[28] = 3
			AIButtons["P1 Down"].pressed = true
			AIButtons["P1 Right"].pressed = true				
		elseif combo_move_step[28] == 3 then -- Continuing this combo move
			combo_move_step[28] = 4
			AIButtons["P1 Right"].pressed = true				
		elseif combo_move_step[28] == 4 then -- Continuing this combo move
			combo_move_step[28] = 5
			AIButtons["P1 Roundhouse Kick"].pressed = true
		elseif combo_move_step[28] == 5 then -- Cooldown frame		
			alldone = true		
		end



	end



	-- If we have finished a combo move, reset all combo move steps
	if alldone then
		combo_move_step[11] = -1
		combo_move_step[12] = -1
		combo_move_step[13] = -1
		combo_move_step[14] = -1
		combo_move_step[15] = -1
		combo_move_step[16] = -1
		combo_move_step[17] = -1
		combo_move_step[18] = -1
		combo_move_step[19] = -1
		combo_move_step[20] = -1
		combo_move_step[21] = -1
		combo_move_step[22] = -1
		combo_move_step[23] = -1
		combo_move_step[24] = -1
		combo_move_step[25] = -1
		combo_move_step[26] = -1
		combo_move_step[27] = -1
		combo_move_step[28] = -1
	end
end


--
-- ProcessAI - Process the AI based on the current game state
--
function ProcessAI()
	-- Clear the state of each button
	AIButtons["P1 Up"].pressed = false
	AIButtons["P1 Down"].pressed = false
	AIButtons["P1 Left"].pressed = false
	AIButtons["P1 Right"].pressed = false
	AIButtons["P1 Jab Punch"].pressed = false
	AIButtons["P1 Strong Punch"].pressed = false
	AIButtons["P1 Fierce Punch"].pressed = false
	AIButtons["P1 Short Kick"].pressed = false
	AIButtons["P1 Forward Kick"].pressed = false
	AIButtons["P1 Roundhouse Kick"].pressed = false


	if P1_Enabled() then
		-- Feed the neural network input data
		-- Some input data is intentionaly inflated to provide bias of importance for that input (ex. facing direction)
		NN:SetInput(1,P1_GetHealth()) -- Range 0-144
		--NN:SetInput(2,P1_GetPositionX())
		--NN:SetInput(3,P1_GetPositionY())
		NN:SetInput(2,P2_GetHealth()) -- Range 0-144
		--NN:SetInput(5,P2_GetPositionX())
		--NN:SetInput(6,P2_GetPositionY())
		--NN:SetInput(3,GetTimer())
		if P1_GetPositionX() < P2_GetPositionX() then	-- Player 1 facing right
			NN:SetInput(3,500)
		else -- Player 1 facing left
			NN:SetInput(3,0)
		end

		-- Each character has a unique set of moves, hitboxes, and AI routines, therefore
		-- several input nodes will be created to help the network learn to handle them
		-- Currently P1 will always be Ryu, but more could be added
		if P2_GetCharacter() == 0 then NN:SetInput(4, 400) else NN:SetInput(4, 0) end -- P2 is Ryu
		if P2_GetCharacter() == 1 then NN:SetInput(5, 400) else NN:SetInput(5, 0) end -- P2 is E Honda
		if P2_GetCharacter() == 2 then NN:SetInput(6, 400) else NN:SetInput(6, 0) end -- P2 is Blanka
		if P2_GetCharacter() == 3 then NN:SetInput(7, 400) else NN:SetInput(7, 0) end -- P2 is Guile
		if P2_GetCharacter() == 4 then NN:SetInput(8, 400) else NN:SetInput(8, 0) end -- P2 is Ken
		if P2_GetCharacter() == 5 then NN:SetInput(9, 400) else NN:SetInput(9, 0) end -- P2 is Chun Li
		if P2_GetCharacter() == 6 then NN:SetInput(10, 400) else NN:SetInput(10, 0) end -- P2 is Zangief
		if P2_GetCharacter() == 7 then NN:SetInput(11, 400) else NN:SetInput(11, 0) end -- P2 is Dhalsim
		if P2_GetCharacter() == 8 then NN:SetInput(12, 400) else NN:SetInput(12, 0) end -- P2 is M Bison
		if P2_GetCharacter() == 9 then NN:SetInput(13, 400) else NN:SetInput(13, 0) end -- P2 is Sagat
		if P2_GetCharacter() == 10 then NN:SetInput(14, 400) else NN:SetInput(14, 0) end -- P2 is Balrog
		if P2_GetCharacter() == 11 then NN:SetInput(15, 400) else NN:SetInput(15, 0) end -- P2 is Vega

--[[
	Character state table:
	0x00 = Standing
	0x02 = Crouching 
	0x04 = Jumping
	0x06 = Jump-crouch
	0x08 = Blocking
	0x0A = Attacking
	0x0E = Stunned
	0x14 = On Ground
--]]

		-- Similarly, there is no linear relationship between states, so several input nodes will be
		-- created to help the network learn how to handle various character states
		if P1_GetState() == 0 then NN:SetInput(16, 200) else NN:SetInput(16, 0) end -- P1 is standing
		if P1_GetState() == 2 then NN:SetInput(17, 200) else NN:SetInput(17, 0) end -- P1 is crouching
		if P1_GetState() == 4 then NN:SetInput(18, 200) else NN:SetInput(18, 0) end -- P1 is jumping
		if P1_GetState() == 6 then NN:SetInput(19, 200) else NN:SetInput(19, 0) end -- P1 is jump-crouching
		if P1_GetState() == 8 then NN:SetInput(20, 200) else NN:SetInput(20, 0) end -- P1 is blocking
		if P1_GetState() == 10 then NN:SetInput(21, 200) else NN:SetInput(21, 0) end -- P1 is attacking
		if P1_GetState() == 14 then NN:SetInput(22, 200) else NN:SetInput(22, 0) end -- P1 is stunned
		if P1_GetState() == 20 then NN:SetInput(23, 200) else NN:SetInput(23, 0) end -- P1 is on ground

		if P2_GetState() == 0 then NN:SetInput(24, 200) else NN:SetInput(24, 0) end -- P2 is standing
		if P2_GetState() == 2 then NN:SetInput(25, 200) else NN:SetInput(25, 0) end -- P2 is crouching
		if P2_GetState() == 4 then NN:SetInput(26, 200) else NN:SetInput(26, 0) end -- P2 is jumping
		if P2_GetState() == 6 then NN:SetInput(27, 200) else NN:SetInput(27, 0) end -- P2 is jump-crouching
		if P2_GetState() == 8 then NN:SetInput(28, 200) else NN:SetInput(28, 0) end -- P2 is blocking
		if P2_GetState() == 10 then NN:SetInput(29, 200) else NN:SetInput(29, 0) end -- P2 is attacking
		if P2_GetState() == 14 then NN:SetInput(30, 200) else NN:SetInput(30, 0) end -- P2 is stunned
		if P2_GetState() == 20 then NN:SetInput(31, 200) else NN:SetInput(31, 0) end -- P2 is on ground


		-- Distance between characters
		NN:SetInput(32, math.abs(P1_GetPositionX() - P2_GetPositionX())) -- Range 0-384 (< screen width)
		NN:SetInput(33, math.abs(P1_GetPositionY() - P2_GetPositionY())) -- Range 0-224 (< screen height)

		for i = 1, 8 do -- 8 projectiles
			if ProjectileActive(i-1) then
				NN:SetInput(((i-1) * 3) + 34, 500)	-- Projectile Active
				--NN:SetInput(((i-1) * 3) + 11, ProjectilePositionX(i-1))	-- Projectile X
				--NN:SetInput(((i-1) * 3) + 12, ProjectilePositionY(i-1))	-- Projectile Y
				NN:SetInput(((i-1) * 3) + 35, 384 - math.abs(P1_GetPositionX() - ProjectilePositionX(i-1)))	-- Projectile X Distance/Closeness			
				NN:SetInput(((i-1) * 3) + 36, 224 - math.abs(P1_GetPositionY() - ProjectilePositionY(i-1)))	-- Projectile Y Distance/Closeness			
			else
				NN:SetInput(((i-1) * 3) + 34, 0)	-- Projectile Inactive
				--NN:SetInput(((i-1) * 3) + 11, 0)	-- Projectile X
				--NN:SetInput(((i-1) * 3) + 12, 0)	-- Projectile Y
				NN:SetInput(((i-1) * 3) + 35, 0) -- Projectile X Distance
				NN:SetInput(((i-1) * 3) + 36, 0) -- Projectile Y Distance
			end
		end



		NN:Update()
		

		-- Get the output states, compare to sensitivity
		-- Special combo moves override any other button presses, so check for those first
		combo_move_index = CheckComboMoves()
		if combo_move_index ~= -1 then
			ProcessComboMoves(combo_move_index)
		else
			if NN:GetOutput(1) > sensitivity then
				AIButtons["P1 Up"].pressed = true
			end
			if NN:GetOutput(2) > sensitivity then
				AIButtons["P1 Down"].pressed = true
			end
			if NN:GetOutput(3) > sensitivity then
				AIButtons["P1 Left"].pressed = true
			end
			if NN:GetOutput(4) > sensitivity then
				AIButtons["P1 Right"].pressed = true
			end
			if NN:GetOutput(5) > sensitivity then
				AIButtons["P1 Jab Punch"].pressed = true
			end
			if NN:GetOutput(6) > sensitivity then
				AIButtons["P1 Strong Punch"].pressed = true
			end
			if NN:GetOutput(7) > sensitivity then
				AIButtons["P1 Fierce Punch"].pressed = true
			end
			if NN:GetOutput(8) > sensitivity then
				AIButtons["P1 Short Kick"].pressed = true
			end
			if NN:GetOutput(9) > sensitivity then
				AIButtons["P1 Forward Kick"].pressed = true
			end
			if NN:GetOutput(10) > sensitivity then
				AIButtons["P1 Roundhouse Kick"].pressed = true
			end
			if NN:GetOutput(11) > sensitivity then -- fireball right (jab)
				combo_move_step[11] = 1
			end
			if NN:GetOutput(12) > sensitivity then -- dragon punch right (jab)
				combo_move_step[12] = 1
			end
			if NN:GetOutput(13) > sensitivity then -- hurricane kick right (short)
				combo_move_step[13] = 1
			end
			if NN:GetOutput(14) > sensitivity then -- fireball left (jab)
				combo_move_step[14] = 1
			end
			if NN:GetOutput(15) > sensitivity then -- dragon punch left (jab)
				combo_move_step[15] = 1
			end
			if NN:GetOutput(16) > sensitivity then -- hurricane kick left (short)
				combo_move_step[16] = 1
			end
			if NN:GetOutput(17) > sensitivity then -- fireball right (strong)
				combo_move_step[17] = 1
			end
			if NN:GetOutput(18) > sensitivity then -- dragon punch right (strong)
				combo_move_step[18] = 1
			end
			if NN:GetOutput(19) > sensitivity then -- hurricane kick right (forward)
				combo_move_step[19] = 1
			end
			if NN:GetOutput(20) > sensitivity then -- fireball left (strong)
				combo_move_step[20] = 1
			end
			if NN:GetOutput(21) > sensitivity then -- dragon punch left (strong)
				combo_move_step[21] = 1
			end
			if NN:GetOutput(22) > sensitivity then -- hurricane kick left (forward)
				combo_move_step[22] = 1
			end
			if NN:GetOutput(23) > sensitivity then -- fireball right (fierce)
				combo_move_step[23] = 1
			end
			if NN:GetOutput(24) > sensitivity then -- dragon punch right (fierce)
				combo_move_step[24] = 1
			end
			if NN:GetOutput(25) > sensitivity then -- hurricane kick right (roundhouse)
				combo_move_step[25] = 1
			end
			if NN:GetOutput(26) > sensitivity then -- fireball left (fierce)
				combo_move_step[26] = 1
			end
			if NN:GetOutput(27) > sensitivity then -- dragon punch left (fierce)
				combo_move_step[27] = 1
			end
			if NN:GetOutput(28) > sensitivity then -- hurricane kick left (roundhouse)
				combo_move_step[28] = 1
			end			

		end
	end

	-- Check the state of each button and send the result to MAME
	-- Joystick
	if AIButtons["P1 Up"].pressed then
		AIButtons["P1 Up"].button:set_value(1)
	else
		AIButtons["P1 Up"].button:set_value(0)
	end
	if AIButtons["P1 Down"].pressed then
		AIButtons["P1 Down"].button:set_value(1)
	else
		AIButtons["P1 Down"].button:set_value(0)
	end
	if AIButtons["P1 Left"].pressed then
		AIButtons["P1 Left"].button:set_value(1)
	else
		AIButtons["P1 Left"].button:set_value(0)
	end
	if AIButtons["P1 Right"].pressed then
		AIButtons["P1 Right"].button:set_value(1)
	else
		AIButtons["P1 Right"].button:set_value(0)
	end

	-- Punches
	if AIButtons["P1 Jab Punch"].pressed then
		AIButtons["P1 Jab Punch"].button:set_value(1)
	else
		AIButtons["P1 Jab Punch"].button:set_value(0)
	end
	if AIButtons["P1 Strong Punch"].pressed then
		AIButtons["P1 Strong Punch"].button:set_value(1)
	else
		AIButtons["P1 Strong Punch"].button:set_value(0)
	end
	if AIButtons["P1 Fierce Punch"].pressed then
		AIButtons["P1 Fierce Punch"].button:set_value(1)
	else
		AIButtons["P1 Fierce Punch"].button:set_value(0)
	end

	-- Kicks
	if AIButtons["P1 Short Kick"].pressed then
		AIButtons["P1 Short Kick"].button:set_value(1)
	else
		AIButtons["P1 Short Kick"].button:set_value(0)
	end
	if AIButtons["P1 Forward Kick"].pressed then
		AIButtons["P1 Forward Kick"].button:set_value(1)
	else
		AIButtons["P1 Forward Kick"].button:set_value(0)
	end
	if AIButtons["P1 Roundhouse Kick"].pressed then
		AIButtons["P1 Roundhouse Kick"].button:set_value(1)
	else
		AIButtons["P1 Roundhouse Kick"].button:set_value(0)
	end


	-- Check for the end of the round
	if GetRoundDone() then
		if not endround_processed then	-- Be sure to process the end of the round only once
			x = GetRoundWinner()
			if x == 1 then -- Player 1 Wins Round
				-- Increase fitness of current genome for winning
				fitness[current_genome] = fitness[current_genome] + 1000

				if P1_GetHealth() == 144 then
					-- Large extra bonus for a perfect round
					fitness[current_genome] = fitness[current_genome] + 250
				else
					-- Decrease fitness for damage taken
					health = P1_GetHealth()
					if health == 65535 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
						health = 0
					end
					fitness[current_genome] = fitness[current_genome] - (2 * (144 - health))
				end

				P1Wins = P1Wins + 1
				generation_wins = generation_wins + 1
			elseif x == 2 then -- Player 2 Wins Round
				-- Reduce fitness of current genome for losing
				fitness[current_genome] = fitness[current_genome] - 288

				-- Increase fitness for damage done 
				health = P2_GetHealth()
				if health == 65535 then -- As soon as the round is over it reads as 65535 (probably a sign issue)
					health = 0
				end
				fitness[current_genome] = fitness[current_genome] + (144 - health)

				-- Increase fitness of current genome for time survived
				fitness[current_genome] = fitness[current_genome] + (153 - GetTimer()) -- 153 "ticks" in a round (hex 99 display "seconds")				

				P2Wins = P2Wins + 1
				generation_losses = generation_losses + 1
			elseif x == 255 then -- Round Draw
				-- Increase fitness of current genome for time survived
				fitness[current_genome] = fitness[current_genome] + (153 - GetTimer()) -- 153 "ticks" in a round (hex 99 display "seconds")				
				
				generation_draws = generation_draws + 1
			end


			print("Generation " .. current_generation .. " Genome " .. current_genome .. " Fitness: " .. fitness[current_genome])

			if P1Wins == 2 or P2Wins == 2 then	-- Check for end of match				
				-- Increment the current genome
				current_genome = current_genome + 1

				-- If this was the last genome to be processed, start a new generation
				if current_genome > total_genomes then
					-- Store fitness and genome data in our genetic algorithm
					GA:StoreFitness(fitness)
					GA:StoreGenomes(genomes)

					-- Print some statistics
					total_fitness = 0
					for i = 1, #fitness do
						total_fitness = total_fitness + fitness[i]
					end
					print("Generation " .. current_generation .. " W/L/D: " .. generation_wins .. "/" .. generation_losses .. "/" .. generation_draws)
					print("Average fitness: " .. total_fitness / total_genomes)


					-- Breed a new generation
					print("Breeding new Generation...")
					genomes = GA:Breed(num_fitparents, num_randomparents, mutationrate)
					fitness = {}

					current_generation = current_generation + 1
					current_genome = 1
					generation_wins = 0
					generation_draws = 0
					generation_losses = 0

					-- Save at the beginning of each generation
					SaveGenomes(savefile)
				end

				-- Load the next genome into the neural network
				NN:Deserialize(genomes[current_genome])
				fitness[current_genome] = 0

				P1Wins = 0
				P2Wins = 0
			end


			endround_processed = true
		end
	else
		endround_processed = false
	end
end





--
-- MAME Operation
--

--
-- main() - Callback for when MAME has finished processing a frame of the emulated machine
--
function main()
	if game_started then
		if frame_num % frame_throttle == 0 then	-- Throttle AI processing
			ProcessAI()

			-- Save the health this frame for each character
			P1PreviousHealth = P1_GetHealth()
			P2PreviousHealth = P2_GetHealth()
		end

		if continue_game then
			-- Insert a coin
			if frame_continue == 0 then
				buttons0["Coin 1"]:set_value(1)
			end
			if frame_continue == 2 then
				buttons0["Coin 1"]:set_value(0)
			end

			-- Press the start button
			if frame_continue == 3 then
				buttons0["1 Player Start"]:set_value(1)
			end
			if frame_continue == 4 then
				buttons0["1 Player Start"]:set_value(0)
				continue_game = false
			end

			frame_continue = frame_continue + 1
		else -- not already in the process of continuing
			-- Check for continue screen
			if GetContinueTime() > 0 then
				-- Insert a coin
				continue_game = true -- Set a flag that we are in the process of continuing a game
				frame_continue = 0 -- Keep track of the number of frames elapsed after starting our process of continuing
			end
		end

	else -- if NOT game_started
		-- Insert one coin when the title screen appears
		-- 1024 frames is more than enough to skip the loading screens
		-- Note, the coin button press appears to require two frames to detect!
		if (frame_num - frame_randomstart) == 1024 then
			buttons0["Coin 1"]:set_value(1)
		end
		if (frame_num - frame_randomstart) == 1026 then
			buttons0["Coin 1"]:set_value(0)
		end

		-- Start a one player game
		if (frame_num - frame_randomstart) == 1150 then
			buttons0["1 Player Start"]:set_value(1)
		end
		if (frame_num - frame_randomstart) == 1151 then
			buttons0["1 Player Start"]:set_value(0)
		end
	
		-- Select Ryu from character select screen
		if (frame_num - frame_randomstart) == 1250 then
			buttons0["1 Player Start"]:set_value(1)
		end
		if (frame_num - frame_randomstart) == 1251 then
			buttons0["1 Player Start"]:set_value(0)
			game_started = true
		end
		
	end -- if NOT game_started


	frame_num = frame_num + 1	
end


--
-- draw_hud() - Callback for when MAME has finished drawing a frame of the emulated machine
--
function draw_hud()
	-- All colors in ARGB format
	local color_green = 0xFF00FF00
	local color_red = 0xFFFF0000
	local color_cyan = 0xFF00FFFF
	local color_black = 0xFF000000
	local color_clear = 0x00000000
	local str = ""
	local x = 0

--[[]
	if P1_Enabled() then
		manager:machine().screens[":screen"]:draw_text(0, 0, "P1 Enabled", color_green)
	else
		manager:machine().screens[":screen"]:draw_text(0, 0, "P1 Disabled", color_green)
	end

	if P2_Enabled() then
		manager:machine().screens[":screen"]:draw_text(0, 8, "P2 Enabled", color_green)
	else
		manager:machine().screens[":screen"]:draw_text(0, 8, "P2 Disabled", color_green)
	end
--]]

	str = "Generation: " .. current_generation .. "  Genome: " .. current_genome
	manager:machine().screens[":screen"]:draw_text(0, 0, str, color_cyan)

	-- Calculate an average fitness for this generation, only including completed genomes
	avg = 0
	if current_genome > 1 then
		for i = 1, current_genome - 1 do
			avg = avg + fitness[i]
		end
		avg = avg / (current_genome - 1)
	end

	str = "Avg. Fitness: " .. string.format("%.2f", avg) 
	manager:machine().screens[":screen"]:draw_text(0, 8, str, color_cyan)

	str = "W/L/D: " .. generation_wins .. "/" .. generation_losses .. "/" .. generation_draws
	manager:machine().screens[":screen"]:draw_text(72, 8, str, color_cyan)

	str = "P1 Health: " .. P1_GetHealth()
	manager:machine().screens[":screen"]:draw_text(0, 200, str, color_red)

	str = "P1 Pos: " .. P1_GetPositionX() .. ", " .. P1_GetPositionY()
	manager:machine().screens[":screen"]:draw_text(48, 200, str, color_red)

	str = "P1 State: " .. P1_GetState()
	manager:machine().screens[":screen"]:draw_text(112, 200, str, color_red)

	str = "P2 Health: " .. P2_GetHealth()
	manager:machine().screens[":screen"]:draw_text(0, 208, str, color_red)

	str = "P2 Pos: " .. P2_GetPositionX() .. ", " .. P2_GetPositionY()
	manager:machine().screens[":screen"]:draw_text(48, 208, str, color_red)

	str = "P2 State: " .. P2_GetState()
	manager:machine().screens[":screen"]:draw_text(112, 208, str, color_red)


	str = "Timer: " .. string.format("%x", GetTimer()) -- Format as hex for a friendly number of game "seconds"
	manager:machine().screens[":screen"]:draw_text(0, 216, str, color_red)

	if game_started then
		if GetRoundDone() then
			manager:machine().screens[":screen"]:draw_text(48, 216, "Round Over!", color_red)
			x = GetRoundWinner()
			if x == 1 then
				manager:machine().screens[":screen"]:draw_text(96, 216, "Player 1 Wins!", color_red)
			elseif x == 2 then
				manager:machine().screens[":screen"]:draw_text(96, 216, "Player 2 Wins!", color_red)				
			elseif x == 255 then
				manager:machine().screens[":screen"]:draw_text(96, 216, "Round Draw!", color_red)
			end
		else
			manager:machine().screens[":screen"]:draw_text(48, 216, "Round In Progress!", color_red)
		end
	else
			manager:machine().screens[":screen"]:draw_text(48, 216, "Game Not Started!", color_red)		
	end



	-- Check the state of each button and draw on screen
	-- Joystick
	if AIButtons["P1 Up"].pressed then
		manager:machine().screens[":screen"]:draw_box(286, 192, 302, 200, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(290, 192, "Up", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(286, 192, 302, 200, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(290, 192, "Up", color_green)
	end
	if AIButtons["P1 Down"].pressed then
		manager:machine().screens[":screen"]:draw_box(286, 212, 302, 220, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(290, 212, "Dn", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(286, 212, 302, 220, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(290, 212, "Dn", color_green)
	end
	if AIButtons["P1 Left"].pressed then
		manager:machine().screens[":screen"]:draw_box(268, 202, 284, 210, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(272, 202, "Lt", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(268, 202, 284, 210, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(272, 202, "Lt", color_green)
	end
	if AIButtons["P1 Right"].pressed then
		manager:machine().screens[":screen"]:draw_box(304, 202, 320, 210, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(308, 202, "Rt", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(304, 202, 320, 210, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(308, 202, "Rt", color_green)
	end

	-- Punches
	if AIButtons["P1 Jab Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(324, 198, 340, 206, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(328, 198, "JP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(324, 198, 340, 206, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(328, 198, "JP", color_green)	
	end
	if AIButtons["P1 Strong Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(344, 198, 360, 206, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(348, 198, "SP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(344, 198, 360, 206, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(348, 198, "SP", color_green)
	end
	if AIButtons["P1 Fierce Punch"].pressed then
		manager:machine().screens[":screen"]:draw_box(364, 198, 380, 206, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(368, 198, "FP", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(364, 198, 380, 206, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(368, 198, "FP", color_green)
	end

	-- Kicks
	if AIButtons["P1 Short Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(324, 210, 340, 218, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(328, 210, "SK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(324, 210, 340, 218, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(328, 210, "SK", color_green)	
	end
	if AIButtons["P1 Forward Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(344, 210, 360, 218, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(348, 210, "FK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(344, 210, 360, 218, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(348, 210, "FK", color_green)		
	end
	if AIButtons["P1 Roundhouse Kick"].pressed then
		manager:machine().screens[":screen"]:draw_box(364, 210, 380, 218, color_green, color_black)
		manager:machine().screens[":screen"]:draw_text(368, 210, "RK", color_black)
	else
		manager:machine().screens[":screen"]:draw_box(364, 210, 380, 218, color_clear, color_green)
		manager:machine().screens[":screen"]:draw_text(368, 210, "RK", color_green)
	end	



	-- Projectile information display
	for i = 1, 8 do -- 8 projectiles
		if ProjectileActive(i-1) then
			str = "Projectile " .. i .. ": " .. ProjectilePositionX(i-1) .. ", " .. ProjectilePositionY(i-1)
			manager:machine().screens[":screen"]:draw_text(160, 160 + ((i-1) * 8), str, color_red)
		end
	end
end




--
-- stop() - Callback for when MAME is stopped.  Note that this does NOT execute if you are using
--  "-video none" on the command line and CTRL-C to stop execution!
--
function stop()
	print("Frames Elapsed: " .. frame_num)

	-- Save our genomes to continue next time
	SaveGenomes(savefile)
end












--
-- Genetic Algorithm setup
--

-- If we have previously saved genome data, load it now 
if FileExists(savefile) then
	LoadGenomes(savefile)
end

-- Ready the first genome
NN:Deserialize(genomes[current_genome])
fitness[current_genome] = 0







--
-- Mame Setup
--

-- Setup callbacks
emu.register_frame(main)
emu.register_frame_done(draw_hud, "frame")
emu.register_stop(stop)








